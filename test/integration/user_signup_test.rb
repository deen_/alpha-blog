require "test_helper"

class UserSignupTest < ActionDispatch::IntegrationTest
  test "get sign up form and sign up" do
    get '/signup'
    assert_response :success
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { username: "deen", email: "deen@example.com", password: "password", admin: false } }
      assert_response :redirect
    end
    follow_redirect!
    assert_response :success
    assert_match "deen", response.body
  end
  
  test "get sign up form and reject invalid registration submission" do
    get '/signup'
    assert_response :success
    assert_no_difference 'User.count' do
      post users_path, params: { user: { username: "deen" } }
    end
    assert_match "errors", response.body
    assert_select 'div.alert'
    assert_select 'h4.alert-heading'
  end
end
